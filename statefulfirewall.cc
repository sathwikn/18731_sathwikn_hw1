/*
 * statefulipfilter.{cc,hh} -- Stateful IP-packet filter
 *
 */

#include <click/config.h>
#include <click/confparse.hh>
#include <click/args.hh>
#include "statefulfirewall.hh"
#include <iostream>
#include <fstream>
#include <sstream>
#include <arpa/inet.h>

/* Add header files as required*/
CLICK_DECLS

/*############################ POLICY ############################*/
Policy::Policy(String s, String d, int sp, int dp, int p, int act)
{ 
    sourceip = s;
    destip = d;
    sourceport = sp;
    destport = dp;
    proto = p;
    action = act;
}

//Destructor
Policy::~Policy()
{
}

/* Return a Connection object representing policy */
Connection Policy::getConnection()
{
    Connection conn(sourceip, destip, sourceport, destport, -1, -1, proto, 1);
    return conn;
}

/* Return action for this Policy */
int Policy::getAction()
{
    return action;
}

//Test Function to print a policy
void Policy::printPolicy()
{
    click_chatter("## %s ## %s ## %d ## %d ## %d ## %d ##", sourceip.c_str(), destip.c_str(), sourceport, destport, proto, action);

}

/*############################ CONNECTION ############################*/

Connection::Connection(String s, String d, int sp, int dp, unsigned long seq_s, unsigned long seq_d, int pr, int fwdflag)
{
    sourceip = s;
    destip = d;
    sourceport = sp;
    destport = dp;
    proto = pr;
    sourceseq = seq_s;
    destseq = seq_d;
    int handshake_stat;
    int finish_status = FIN_UNINIT;
    int isfw;
    //click_chatter("src ip - %s dst ip - %s", s.c_str(), d.c_str());
}

Connection::Connection()
{
}

Connection::~Connection()
{

}

void Connection::printCon()
{
    click_chatter("## %s ## %s ## %d ## %d ## %d ## %d ##", sourceip.c_str(), destip.c_str(), sourceport, destport, proto, isfw);
}

/* Overload == operator to check if two Connection objects are equal.
 * You may or may not want to ignore the isfw flag for comparison depending on your implementation.
 * Return true if equal. false otherwise. */
bool Connection::operator==(const Connection &other) const
{
    //click_chatter("printing both the src ips being compared: %s and %s", this->sourceip.c_str(), other.sourceip.c_str());
    //click_chatter("printing both the dest ips being compared: %s and %s", this->destip.c_str(), other.destip.c_str());
    //Check if source and dest ip's of both the connections are the same
    if ((strcmp(this->sourceip.c_str(), other.sourceip.c_str()) == 0) && (strcmp(this->destip.c_str(), other.destip.c_str()) == 0))
    {
        //Check if source and dest ports are the same
        if (this->sourceport == other.sourceport && this->destport == other.destport)
        {
            //Check if the protocol is the same
            if (this->proto == other.proto)
            {
                return true;
            }
            else
            {
                return false;
            }

        }
        else
        {
            return false;
        }
    }
    else
    {
        return false;
    }

}

//Compare two connections to determine the sequence in map.
int Connection::compare(const Connection other) const
{
    //click_chatter("Source ip: %s, other source ip: %s, return value: %d",sourceip.c_str(), other.sourceip.c_str(), strcmp(sourceip.c_str(), other.sourceip.c_str()));
    //Compare src ip's
    if (strcmp(sourceip.c_str(), other.sourceip.c_str()) == 0)
    {
        return sourceport - other.sourceport;
    }
    else
    {
        return sourceip.compare(other.sourceip);
    }   
}

unsigned long Connection::get_sourceseq() 
{
    return sourceseq;
}

unsigned long Connection::get_destseq()
{
    return destseq;
}

void Connection::set_sourceseq( unsigned long seq_s )
{
    sourceseq = seq_s;
}

void Connection::set_destseq( unsigned long seq_d )
{
    destseq = seq_d;
}

int Connection::get_handshake_stat()
{
    return handshake_stat;
}

/* Update the status of the handshake */
void Connection::update_handshake_stat(int status)
{
    handshake_stat = status;
}

int Connection::get_finish_status()
{
    return finish_status;
}

void Connection::update_finish_status(int fin_status)
{
    finish_status = fin_status;
}

bool Connection::is_forward()
{
    return isfw;
}

/*############################ STATEFUL FIREWALL ############################*/

//Constructor
StatefulFirewall::StatefulFirewall()
{

}

//Destructor
StatefulFirewall::~StatefulFirewall()
{

}

/* Take the configuration parameters as input corresponding to
 * POLICYFILE and DEFAULT where
 * POLICYFILE : Path of policy file
 * DEFAULT : Default action (0/1)
 *
 * Hint: Refer to configure methods in other elemsnts.*/
int StatefulFirewall::configure(Vector<String> &conf, ErrorHandler *errh)
{
    if (Args(conf, this, errh).read("POLICYFILE", POLICY_FILE).read("DEFAULT", DEFAULTACTION).complete() < 0)
        return -1;

    read_policy_config(POLICY_FILE);

    return 0;
}

/* Read policy from a config file whose path is passed as parameter.
 * Update the policy database.
 * Policy config file structure would be space separated list of
 * <source_ip source_port destination_ip destination_port protocol action>
 * Add Policy objects to the list_of_policies
 * */
int StatefulFirewall::read_policy_config(String POLICY_FILE)
{
    const char* sourceip;
    const char* destip;
    int sourceport;
    int destport;
    int proto;
    int action;

    ifstream infile(POLICY_FILE.c_str());
    string line;
    if (infile.is_open())
    {
        while (getline(infile, line) && line.length() > 0)
        {
            if (line[0] == '#')
            {
                //Ignore the comment line
                continue;
            }
            else
            {
                //click_chatter("POLICY: %s", line.c_str());
                //Process data on other lines and store it in the vector
                string arr[6];
                int i = 0;
                stringstream ssin(line);
                while (ssin.good() && i < 6)
                {
                    ssin >> arr[i];
                    i++;
                }
                sourceip = arr[0].c_str();
                destip = arr[2].c_str();
                sourceport = atoi(arr[1].c_str());
                destport = atoi(arr[3].c_str());
                proto = atoi(arr[4].c_str());
                action = atoi(arr[5].c_str());
 
                Policy newPolicy(String(sourceip), String(destip), sourceport, destport, proto, action);
                list_of_policies.push_back(newPolicy);
            }

        }
        infile.close();
    }
    else
    {
        cout << "Error opening policy file" << endl;
    }
    /*Printing policies for verification
    vector<Policy>::iterator it; 
    for (it = list_of_policies.begin(); it != list_of_policies.end(); it++)
    {       
        click_chatter("printing policies");
        it->printPolicy();
             
    }*/
}

/* Check if Packet belongs to new connection.
 * If new connection, apply the policy on this packet
 * and add the result to the connection map.
 * Else return the action in map.
 * If Packet indicates connection reset,
 * delete the connection from connection map.
 *
 * Return 1 if packet is allowed to pass
 * Return 0 if packet is to be discarded
 */
int StatefulFirewall::filter_packet(const Packet *pkt)
{
    /*click_chatter("In filter_packet function");
    static int i = 0;
    i++;
    click_chatter("\n packet number - ");*/
    

    //Call get_canonicalized_connection function to create a Connection object
    Connection conn = get_canonicalized_connection(pkt);
    const click_ip *ipHdr = pkt->ip_header();
    const click_tcp *tcpHdr;

    //If the packet is not of TCP/UDP/ICMP or of IPv6 simply return the default action
    if ((!(ipHdr->ip_p == IP_PROTO_TCP) && !(ipHdr->ip_p == IP_PROTO_UDP) && !(ipHdr->ip_p == IP_PROTO_ICMP)) || (ipHdr->ip_v == 6))
    {
        return DEFAULTACTION;
    }

    //Check if there exists an entry in the map
    if (check_if_new_connection(conn))
    {
        //TCP
        if (ipHdr->ip_p == IP_PROTO_TCP)
        {
            //click_chatter("Inside TCP");
            tcpHdr = pkt->tcp_header();
            //SYN packet
            if ((tcpHdr->th_flags & TH_SYN) && !(tcpHdr->th_flags & TH_ACK))
            {
                conn.update_handshake_stat(SYN);
            }
            else
            {
                return DEFAULTACTION;
            }
        }

        //click_chatter("It is a new connection");
        vector<Policy>::iterator it; 
        //Apply policy and add to map
        //click_chatter("POLICY CHECKS START");
        for (it = list_of_policies.begin(); it != list_of_policies.end(); it++)
        {
            if (conn == it->getConnection())
            {
                add_connection(conn, it->getAction());
                        //click_chatter("Added a new connection to map");
                return it->getAction();
            }
        }
        //click_chatter("POLICY CHECKS END");

        //If there is no corresponding policy add it to the map with default action
        add_connection(conn, DEFAULTACTION);
        //click_chatter("Added a new connection to map with default policy");
        return DEFAULTACTION;
    }
    //If it is an old connection return action from map
    else
    {
        //click_chatter("It is an existing connection");
        int action = 0;
        std::map<Connection,int, cmp_connection>::iterator it;

        it = Connections.find(conn);
        if (it != Connections.end())
        {
            Connection oldConn = it->first;
            //click_chatter("Found connection. Returning action from an existing connection");

            //TCP
            if (ipHdr->ip_p == IP_PROTO_TCP)
            {
                tcpHdr = pkt->tcp_header();
                //If RST flag is set delete the connection from map
                if (check_if_connection_reset(pkt))
                {
                    action = it->second;
                    delete_connection(conn);
                }
                //If connection is established return action without any further checks
                else if(oldConn.get_handshake_stat() == HS_DONE)
                {
                    action = it->second;

                    //Handle FIN
                    if ((oldConn.get_finish_status() == FIN_UNINIT) && (tcpHdr->th_flags & TH_FIN) && !(tcpHdr->th_flags & TH_ACK))
                    {
                        conn.update_finish_status(0);
                        delete_connection(oldConn);
                        add_connection(conn, action);
                    }
                    else if ((oldConn.get_finish_status() == FIN_INIT) && (tcpHdr->th_flags & TH_FIN) && (tcpHdr->th_flags & TH_ACK))
                    {
                        if ((conn.is_forward() == 0 && conn.get_destseq() == (oldConn.get_destseq() + 1)) || (conn.is_forward() == 1 && conn.get_sourceseq() == (oldConn.get_sourceseq() + 1)))
                        {
                            conn.update_finish_status(1);
                            delete_connection(oldConn);
                            add_connection(conn, action);
                        }
                    }
                    else if ((oldConn.get_finish_status() == FIN_ACK) && !(tcpHdr->th_flags & TH_FIN) && (tcpHdr->th_flags & TH_ACK))
                    {
                        if ((conn.is_forward() == 0 && conn.get_sourceseq() == (oldConn.get_sourceseq() + 1)) || (conn.is_forward() == 1 && conn.get_destseq() == (oldConn.get_destseq() + 1)))
                        {
                            delete_connection(oldConn);
                        }
                    }

                }
                //If you are in SYN state and receive a duplicate SYN, block that connection as it may be a SYN FLOOD
                else if ((oldConn.get_handshake_stat() == SYN) && (tcpHdr->th_flags & TH_SYN) && !(tcpHdr->th_flags & TH_ACK))
                {
                    action = 0;
                    delete_connection(oldConn);
                    add_connection(conn, action);
                }
                //If we are in SYN state and receive a corresponding SYN ACK
                else if ((oldConn.get_handshake_stat() == SYN) && (tcpHdr->th_flags & TH_SYN) && (tcpHdr->th_flags & TH_ACK))
                {
                    //check if ACK = SYN(client) + 1, then it is SYN ACK
                    if ((conn.is_forward() == 0 && conn.get_destseq() == (oldConn.get_destseq() + 1)) || (conn.is_forward() == 1 && conn.get_sourceseq() == (oldConn.get_sourceseq() + 1)))
                    {
                        action = it->second;
                        //delete the connection and re-add it to map to update the handshake status
                        conn.update_handshake_stat(SYNACK);
                        delete_connection(oldConn);
                        add_connection(conn, action);
                    }
                    else
                    {
                        //TODO change this to default action and check
                        action = 0;
                    }
                }
                //If we are in SYN ACK state and receive a corresponding ACK
                else if ((oldConn.get_handshake_stat() == SYNACK) && !(tcpHdr->th_flags & TH_SYN) && (tcpHdr->th_flags & TH_ACK))
                {
                    //Check if ACK = SYN(server) + 1, then it is ACK
                    if ((conn.is_forward() == 0 && conn.get_sourceseq() == (oldConn.get_sourceseq() + 1)) || (conn.is_forward() == 1 && conn.get_destseq() == (oldConn.get_destseq() + 1)))
                    {
                        action = it->second;
                        conn.update_handshake_stat(HS_DONE);
                        delete_connection(oldConn);
                        add_connection(conn, action);
                    }
                    else
                    {
                        action = 0;
                    }
                }
                //If we receive an ACK directly after SYN
                else
                {
                    action = 0;
                }

            }
            //For ICMP or UDP just return the action
            else if (ipHdr->ip_p == IP_PROTO_ICMP || ipHdr->ip_p == IP_PROTO_UDP)
            {
                action = it->second;
            }
        }
        return action;
    }
}
    
/* Push valid traffic on port 1
 * Push discarded traffic on port 0*/
void StatefulFirewall::push(int port, Packet *pkt)
{
    if (filter_packet(pkt) == 1)
        output(1).push(pkt);
    else
        output(0).push(pkt);        
}

/* return true if Packet represents a new connection
 * i.e., check if the connection exists in the map.
 * You can also check the SYN flag in the header to be sure.
 * else return false.
 * Hint: Check the connection ID database.
 */
bool StatefulFirewall::check_if_new_connection(Connection conn)
{
    /*printing entries in map
    click_chatter("PRINTING MAP BEFORE NEW CONN CHECK");
    std::map<Connection,int, cmp_connection>::iterator it1;
    for (it1 = Connections.begin(); it1 != Connections.end(); it1++)
    {
        Connection con = it1->first;
        con.printCon();      
    }*/

    //Create an iterator to search in the map
    std::map<Connection,int, cmp_connection>::iterator it;
    it = Connections.find(conn);
    if (it == Connections.end())
    {
        return true;
    }
    return false;
}

/*Check if the packet represent Connection reset
 * i.e., if the RST flag is set in the header.
 * Return true if connection reset
 * else return false.*/
bool StatefulFirewall::check_if_connection_reset(const Packet *pkt)
{
    //Extract the IP header
    const click_ip *ipHdr = pkt->ip_header();
    //Check if it is TCP
    if (ipHdr->ip_p == IP_PROTO_TCP)
    {
        const click_tcp *tcpHdr = pkt->tcp_header();
        if (tcpHdr->th_flags & TH_RST)
            return true;
    }
    return false;
}

/* Add a new connection to the map along with its action.*/
void StatefulFirewall::add_connection(Connection &c, int action)
{
    //click_chatter("In add_connection");
    Connections.insert(std::map<Connection,int>::value_type(c, action));

}

/* Delete the connection from map*/
void StatefulFirewall::delete_connection(Connection &conn)
{
    std::map<Connection,int, cmp_connection>::iterator it;
    it = Connections.find(conn);
    if (it != Connections.end())
    {
        Connections.erase(it);
    }
}

/* Create a new connection object for Packet.
 * Make sure you canonicalize the source and destination ip address and port number.
 * i.e, make the source less than the destination and
 * update isfw to false if you have to swap source and destination.
 * return NULL on error. */
Connection StatefulFirewall::get_canonicalized_connection(const Packet *pkt)
{
    //Extract all the data and create a Connection object
    string srcIP;
    string dstIP;
    int sPort;
    int dPort;
    unsigned long srcSeq;
    unsigned long dstSeq;
    int proto;
    int fwdFlag = 1;

    const click_ip *ipHdr = pkt->ip_header();
    srcIP = inet_ntoa(ipHdr->ip_src);
    dstIP = inet_ntoa(ipHdr->ip_dst);
    sPort = -1;
    dPort = -1;
    srcSeq = -1;
    dstSeq = -1;
    proto = ipHdr->ip_p;

    if (proto == IP_PROTO_TCP)
    {
        const click_tcp *tcpHdr = pkt->tcp_header();
        sPort = ntohs(tcpHdr->th_sport);
        dPort = ntohs(tcpHdr->th_dport);
        srcSeq = ntohl(tcpHdr->th_seq);
        dstSeq = ntohl(tcpHdr->th_ack);
    }
    else if (proto == IP_PROTO_UDP)
    {
        const click_udp *udpHdr = pkt->udp_header();
        sPort = ntohs(udpHdr->uh_sport);
        dPort = ntohs(udpHdr->uh_dport);
    }

    //We need not do anything for ICMP since we have already set the default values to -1

    //Check if the source is less than destination for TCP connections, swap otherwise
    if (proto == IP_PROTO_TCP && strcmp(srcIP.c_str(), dstIP.c_str()) > 0)
    {
        string tempIP = srcIP;
        srcIP = dstIP;
        dstIP = tempIP;
        int tempPort = sPort;
        sPort = dPort;
        dPort = tempPort;
        unsigned long tempSeq = srcSeq;
        srcSeq = dstSeq;
        dstSeq = tempSeq;

        //Set fwdFlag to 0 since we have swapped source and destination
        fwdFlag = 0;
    }

    //Create current connection object
    Connection conn(String(srcIP.c_str()), String(dstIP.c_str()), sPort, dPort, srcSeq, dstSeq, proto, fwdFlag);
    //click_chatter("Source ip: %s and dest ip: %s", String(srcIP.c_str()).c_str(), String(dstIP.c_str()).c_str());
    return conn;
}

/* Convert the integer ip address to string in dotted format.
* Store the string in s.
*
* Hint: ntohl could be useful.*/
void StatefulFirewall::dotted_addr(const uint32_t *addr, char *s)
{
    //I'm using inet_ntoa to do the conversion so I don't need this function
}


CLICK_ENDDECLS
EXPORT_ELEMENT(StatefulFirewall)